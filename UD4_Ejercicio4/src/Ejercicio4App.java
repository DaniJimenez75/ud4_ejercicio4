/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int N = 10;
		//Valor inicial
		System.out.println("Valor inicial de N: "+N);
		
		//INCREMENTO
		N = N+77;
		System.out.println("N+77:"+N);
		
		//DECREMENTO
		N = N-3;
		System.out.println("N-3:"+N);
		
		//DUPICAR
		N = N*2;
		System.out.println("N*2:"+N);

	}

}
